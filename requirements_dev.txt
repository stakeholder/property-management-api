#
# Requirements during development
#

# Production packages
-r requirements.txt

# Development packages
flake8==3.6.0
pytest==3.5.0
factory_boy
sphinx
sphinx_rtd_theme