from api import api
from api.views.tenant import Tenant


api.add_url_rule('/tenants', endpoint='Tenant_post', view_func=Tenant.post,
                 methods=['POST'], strict_slashes=False)
api.add_url_rule('/tenants/<tenant_id>', endpoint='Tenant_put', view_func=Tenant.put,
                 methods=['PUT'], strict_slashes=False)