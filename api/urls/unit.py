from api import api
from api.views.unit import Unit


api.add_url_rule('/properties/<property_id>/units/<unit_id>/add', endpoint='Unit_add_tenant', view_func=Unit.add_tenant,
                 methods=['PUT'], strict_slashes=False)
api.add_url_rule('/properties/<property_id>/units/<unit_id>/remove', endpoint='Unit_remove_tenant', view_func=Unit.remove_tenant,
                 methods=['PUT'], strict_slashes=False)