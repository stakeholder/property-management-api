from api import api
from api.views.property import Property


api.add_url_rule('/properties', endpoint='Property_post', view_func=Property.post,
                 methods=['POST'], strict_slashes=False)
api.add_url_rule('/properties/<property_id>', endpoint='Property_put', view_func=Property.put,
                 methods=['PUT'], strict_slashes=False)
api.add_url_rule('/properties', endpoint='Property_index', view_func=Property.index,
                 methods=['GET'], strict_slashes=False)
api.add_url_rule('/properties/<property_id>/tenants', endpoint='Property_tenants', view_func=Property.tenants,
                 methods=['GET'], strict_slashes=False)