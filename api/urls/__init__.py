from . import (
    user,
    property,
    tenant,
    unit
)
from api import api
from api.views import index

api.add_url_rule("/", endpoint='index', view_func=index, methods=["GET"], strict_slashes=False)
