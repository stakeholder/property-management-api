from api import api
from api.views.user import User


api.add_url_rule('/register', endpoint='User_register', view_func=User.register,
                 methods=['POST'], strict_slashes=False)
api.add_url_rule('/login', endpoint='User_login', view_func=User.login,
                 methods=['POST'], strict_slashes=False)
