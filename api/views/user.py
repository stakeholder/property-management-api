from typing import (
    Any,
    Dict
)
from flask import request

from api.concepts.user.operation import (
    Register,
    Login
)
from api.decorators import catch_exceptions
from api.utils import respond_to_json

class User:
    @classmethod
    @catch_exceptions('Registration Failed')
    def register(cls):
        response: Dict[str, Any] = Register.process(request)
        return respond_to_json(data=response, status=201)

    @classmethod
    @catch_exceptions('Login Failed')
    def login(cls):
        response: Dict[str, Any] = Login.process(request)
        return respond_to_json(data=response, message='login successful')