from typing import (
    Any,
    Dict
)
from flask import request

from api.concepts.property.operation import (
    Post,
    Put,
    Index,
    Tenant
)
from api.decorators import (
    catch_exceptions,
    verify_token
)
from api.utils import respond_to_json

class Property:
    @classmethod
    @catch_exceptions('Failed creating a new property')
    @verify_token
    def post(cls):
        response: Dict[str, Any] = Post.process(request)
        return respond_to_json(data=response, status=201)

    @classmethod
    @catch_exceptions('Failed updating property')
    @verify_token
    def put(cls, property_id):
        response: Dict[str, Any] = Put.process(request, property_id)
        return respond_to_json(data=response)

    @classmethod
    @catch_exceptions('Failed to fetch properties')
    @verify_token
    def index(cls):
        response: Dict[str, Any] = Index.process(request)
        return respond_to_json(data=response)

    @classmethod
    # @catch_exceptions('Failed to fetch tenants')
    @verify_token
    def tenants(cls, property_id):
        response: Dict[str, Any] = Tenant.process(request, property_id)
        return respond_to_json(data=response)
