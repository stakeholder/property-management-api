from typing import (
    Any,
    Dict
)
from flask import request

from api.concepts.tenant.operation import (
    Post,
    Put
)
from api.decorators import (
    catch_exceptions,
    verify_token
)
from api.utils import respond_to_json

class Tenant:
    @classmethod
    @catch_exceptions('Failed creating a new tenant')
    @verify_token
    def post(cls):
        response: Dict[str, Any] = Post.process(request)
        return respond_to_json(data=response, status=201)

    @classmethod
    @catch_exceptions('Failed updating tenant')
    @verify_token
    def put(cls, tenant_id):
        response: Dict[str, Any] = Put.process(request, tenant_id)
        return respond_to_json(data=response)