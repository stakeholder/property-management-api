from typing import (
    Any,
    Dict
)
from flask import request

from api.concepts.unit.operation import Put

from api.decorators import (
    catch_exceptions,
    verify_token
)
from api.utils import respond_to_json

class Unit:
    @classmethod
    @verify_token
    def put(cls, request, property_id, unit_id):
        response: Dict[str, Any] = Put.process(request, property_id, unit_id)
        return response

    @classmethod
    # @catch_exceptions('Failed to add tenant to unit')
    def add_tenant(cls, property_id, unit_id):
        response: Dict[str, Any] = cls.put(request, property_id, unit_id)
        return respond_to_json(data=response)

    @classmethod
    # @catch_exceptions('Failed to remove tenant from unit')
    def remove_tenant(cls, property_id, unit_id):
        response: Dict[str, Any] = cls.put(request, property_id, unit_id)
        return respond_to_json(data=response)