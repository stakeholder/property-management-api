from api.utils import respond_to_json


def index():
    data = {
        'name': 'Property Management Service',
        'status': 'running',
        'version': '0.8.0'
    }
    return respond_to_json(data=data)