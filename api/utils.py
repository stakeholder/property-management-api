import json

from typing import (
    List,
    Dict,
    Any,
    Optional
)

from flask import (
    jsonify,
    request
)

from .exceptions import ValidationException


def is_json(request):
    """
    this function checks if a request is json and also checks if the
    json data sent is not empty
    """
    methods = ['POST', 'PUT', 'PATCH']
    if (request.method in methods and not request.is_json):
        raise ValidationException("Only json request are acceptable", propagate=True)
    elif (request.method in methods) and (not bool(request.data) or json.loads(request.data) == 0):
        raise ValidationException("Empty data is not acceptable", propagate=True)


def get_json(request):
    """
    this function tries to extract data from the json request and raises
    an error if the data is not json serializable
    """
    try:
        data: str = request.json
        return data if not (type(data) == str) else json.loads(data)
    except Exception:
        raise ValidationException("The inputted data is not in json format", propagate=True)


def validate_keys_existence(param: Dict[str, Any], keys: List[str]) -> None:
    missing_keys: List[str] = list(set(keys).difference(set(param.keys())))
    if missing_keys:
        err_msg: str = f'the following key(s) {missing_keys} is/are missing'
        raise ValidationException(err_msg, propagate=True)


def check_for_keys_without_values(param: Dict[str, Any], keys: List[str]) -> None:
    result: List[str] = list(filter(lambda x: not(bool(param.get(x))), keys))
    if result:
        err_msg: str = f'the following key(s) {result} can\'t have empty values'
        raise ValidationException(err_msg, propagate=True)


def check_for_invalid_keys(param: List[str], acceptable_keys: List[str]) -> None:
    invalid_keys: List[str] = list(filter(lambda value: not(value in acceptable_keys), param))
    if invalid_keys:
        error_message: str = f'the following key(s) {invalid_keys} are not acceptable'
        raise ValidationException(error_message, propagate=True)


def respond_to_json(success=True, message="Operation was successful",
                    data=None, status=200):
    return jsonify({
        "success": success,
        "message": message,
        "data": data,
    }), status


def validate_request(request_object: request, required_param_keys: Optional[List[str]] = None,
                     keys_to_cast_to_int: Optional[List[str]] = None,
                     acceptable_param_keys: Optional[List[str]] = None) -> Dict[str, Any]:
    """
    Validates if request meets the following criteria.
    - if it's a json request
    - if it contains the required parameters.
    - if the values of the required parameters are not empty

    :param request_object: A flask request object
    :param required_param_keys: List[str]: A list of required query keys
    :param keys_to_cast_to_int: List[str]: A list of keys to cast to int
    :return: A dictionary containing the request query arguments and their values
    """

    # we verify if the request is of type json
    is_json(request_object)

    if request.method == 'GET':
        request_param: Dict[str, Any] = request_object.args.to_dict()
    elif request.method in ['POST', 'PUT', 'PATCH']:
        request_param: Dict[str, Any] = get_json(request)

    if required_param_keys:
        # we need to validate if required query argument keys are present
        validate_keys_existence(request_param, required_param_keys)

        # we also check if the required keys don't have empty values
        check_for_keys_without_values(request_param, required_param_keys)

    if acceptable_param_keys:
        check_for_invalid_keys(list(request_param.keys()), acceptable_param_keys)

    if keys_to_cast_to_int:
        for key in keys_to_cast_to_int:
            try:
                if key == 'limit':
                    value: Any = request_param.pop(key, 200)
                elif key == 'page':
                    value: Any = request_param.pop(key, 1)
                else:
                    value: Any = request_param.pop(key)
                request_param[key] = int(value)
            except ValueError:
                raise ValidationException(f'{key} value is not an integer', propagate=True)

    return request_param
