from functools import wraps
from typing import (
    Dict,
    Any
)

from flask import (
    request,
    current_app,
    g
)
from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer,
    BadSignature,
    SignatureExpired
)

from api.errors import (
    custom_error,
    validation_error
)
from api.exceptions import ValidationException
from exceptions import CustomException




def catch_exceptions(default_error_message):
    def wrap(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except CustomException as e:
                return custom_error(e.error_message)
            except ValidationException as e:
                if e.should_propagate:
                    return e.broadcast()
                return validation_error(default_error_message)
            except BaseException as e:
                return custom_error(default_error_message)
        return decorated_function
    return wrap

def verify_token(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        token: str = request.headers.get('Token')
        if not token:
            raise ValidationException(f'Invalid token', propagate=True)

        serializer: TimedJSONWebSignatureSerializer = Serializer(current_app.config['SECRET_KEY'])
        try:
            payload: Dict[str, Any] = serializer.loads(token)
            g.payload = payload
        except BadSignature or SignatureExpired:
            raise ValidationException(f'Invalid token', propagate=True)
        return func(*args, **kwargs)
    return decorated_function
