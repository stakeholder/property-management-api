from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    g
)

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import (
    Unit,
    Tenant
)
from exceptions import CustomException


class Put:
    @staticmethod
    def _validate_request_and_params(request_object: request) -> Dict[str, Any]:
        acceptable_param_keys: List[str] = ['tenant_id']
        if request_object.endpoint == 'api.Unit_add_tenant':
            request_param: Dict[str, Any] = validate_request(request_object,
                                                             required_param_keys=['tenant_id'],
                                                             keys_to_cast_to_int=['tenant_id'],
                                                             acceptable_param_keys=acceptable_param_keys)
        else:
            request_param: Dict[str, Any] = validate_request(request_object,
                                                             acceptable_param_keys=acceptable_param_keys)
            request_param.update(tenant_id=None)

        return request_param

    @staticmethod
    def _check_unit_existence(property_id: int, unit_id: int, user_id: int):
        return Unit.query.filter_by(id=unit_id, user_id=user_id, property_id=property_id).first()

    @staticmethod
    def _check_tenant_existence(tenant_id: int, user_id: int):
        return Tenant.query.filter_by(id=tenant_id, user_id=user_id).first()

    @classmethod
    def process(cls, request_object: request, property_id: str, unit_id: str):
        param: Dict[str, int] = cls._validate_request_and_params(request_object)
        try:
            property_id: int = int(property_id)
            unit_id: int = int(unit_id)
        except Exception:
            raise CustomException('Either property_id or unit_id is not an integer')

        if not cls._check_unit_existence(property_id, unit_id, g.payload['id']):
            raise CustomException('Unit does not exist')

        if (request_object.endpoint == 'api.Unit_add_tenant') and (not cls._check_tenant_existence(param['tenant_id'], g.payload['id'])):
            raise CustomException('The tenant you are trying to add does not exist')

        return Unit.update(unit_id, param)
