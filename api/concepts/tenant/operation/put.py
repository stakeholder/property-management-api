from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    g
)

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import Tenant
from exceptions import CustomException


class Put:
    @staticmethod
    def _validate_request_and_params(request_object: request) -> Dict[str, Any]:
        acceptable_param_keys: List[str] = ['firstname', 'lastname']
        request_param: Dict[str, Any] = validate_request(request_object,
                                                         acceptable_param_keys=acceptable_param_keys)

        return request_param

    @staticmethod
    def _check_tenant_existence(tenant_id: int, user_id: int):
        return Tenant.query.filter_by(id=tenant_id, user_id=user_id).first()

    @classmethod
    def process(cls, request_object: request, tenant_id: str):
        param: Dict[str, int] = cls._validate_request_and_params(request_object)
        try:
            tenant_id: int = int(tenant_id)
        except Exception:
            raise CustomException('Tenant id must be an integer')

        if not cls._check_tenant_existence(tenant_id, g.payload['id']):
            raise CustomException('Tenant does not exist')
        return Tenant.update(tenant_id, param)
