from typing import (
    Dict,
    List,
    Any
)

from flask import request

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import User

class Register:
    @staticmethod
    def _validate_request_and_params(request_object: request) -> Dict[str, Any]:
        required_query_keys: List[str] = ['email', 'password', 'verify_password',
                                          'lastname', 'firstname']
        request_param: Dict[str, Any] = validate_request(request_object,
                                                         required_param_keys=required_query_keys)
        return request_param

    @classmethod
    def _verify_email_availability(cls, email: str):
        return User.is_email_available(email)

    @classmethod
    def _verify_password_match(cls, password: str, verify_password: str) -> bool:
        return password.strip() == verify_password.strip()

    @classmethod
    def process(cls, request_object: request):
        param: Dict[str, int] = cls._validate_request_and_params(request_object)

        if not cls._verify_email_availability(param.get('email')):
            error_message: str = f'this email {param.get("email")} has been taken'
            raise ValidationException(error_message, propagate=True)

        if not cls._verify_password_match(param.get('password'), param.get('verify_password')):
            error_message: str = f'Password and Verify Password are not the same'
            raise ValidationException(error_message, propagate=True)

        user: User = User.create(param)
        return user.to_dict()