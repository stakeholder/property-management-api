from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    current_app
)
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import User

class Login:
    @staticmethod
    def _validate_request_and_params(request_object: request) -> Dict[str, Any]:
        required_query_keys: List[str] = ['email', 'password']
        request_param: Dict[str, Any] = validate_request(request_object,
                                                         required_param_keys=required_query_keys)
        return request_param

    @staticmethod
    def _does_email_exist(email: str) -> bool:
        return not User.is_email_available(email)

    @staticmethod
    def _generate_token(payload: Dict[str, Any]) -> str:
        serializer = Serializer(current_app.config['SECRET_KEY'], expires_in=3600)
        return serializer.dumps(payload).decode(encoding='ascii')

    @classmethod
    def process(cls, request_object: request) -> Dict[str, Any]:
        param: Dict[str, int] = cls._validate_request_and_params(request_object)
        email: str = param.get('email')
        password: str = param.get('password')
        error_message: str = f'Invalid credentials'

        if not cls._does_email_exist(email):
            raise ValidationException(error_message, propagate=True)

        user: User = User.query.filter_by(email=email).first()
        if not user.verify_password(password):
             raise ValidationException(error_message, propagate=True)

        token: str = cls._generate_token({"email": user.email, "id": user.id})
        return {'token': token}




