from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    g
)

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import Property
from exceptions import CustomException


class Index:
    @classmethod
    def process(cls, request_object: request):
        user_id: int =  g.payload['id']
        return Property.index({'user_id': user_id})
