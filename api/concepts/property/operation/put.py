from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    g
)

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import Property
from exceptions import CustomException


class Put:
    @staticmethod
    def _validate_request_and_params(request_object: request) -> Dict[str, Any]:
        acceptable_param_keys: List[str] = ['address']
        request_param: Dict[str, Any] = validate_request(request_object,
                                                         acceptable_param_keys=acceptable_param_keys)

        return request_param

    @staticmethod
    def _check_property_existence(property_id: int, user_id: int):
        return Property.query.filter_by(id=property_id, user_id=user_id).first()

    @classmethod
    def process(cls, request_object: request, property_id: str):
        param: Dict[str, int] = cls._validate_request_and_params(request_object)
        try:
            property_id: int = int(property_id)
        except Exception:
            raise CustomException('Property id must be an integer')

        if not cls._check_property_existence(property_id, g.payload['id']):
            raise CustomException('Property does not exist')
        return Property.update(property_id, param)
