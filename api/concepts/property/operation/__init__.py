from .post import Post
from .put import Put
from .index import Index
from .tenant import Tenant