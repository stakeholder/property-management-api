from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    g
)

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import Property


class Post:
    @staticmethod
    def _validate_request_and_params(request_object: request) -> Dict[str, Any]:
        required_query_keys: List[str] = ['address', 'units']
        request_param: Dict[str, Any] = validate_request(request_object,
                                                         required_param_keys=required_query_keys)

        return request_param



    @classmethod
    def process(cls, request_object: request):
        param: Dict[str, int] = cls._validate_request_and_params(request_object)
        param.update(user_id=g.payload['id'])
        property: Property = Property.create(param)
        return property.to_dict()


