from typing import (
    Dict,
    List,
    Any
)

from flask import (
    request,
    g
)

from api.utils import validate_request
from api.exceptions import ValidationException
from resources.models import Property
from exceptions import CustomException


class Tenant:
    @staticmethod
    def _check_property_existence(property_id: int, user_id: int):
        return Property.query.filter_by(id=property_id, user_id=user_id).first()

    @classmethod
    def process(cls, request_object: request, property_id: str):
        user_id: int =  g.payload['id']
        try:
            property_id: int = int(property_id)
        except Exception:
            raise CustomException('Property id is not an integer')

        property: Property = cls._check_property_existence(property_id, user_id)
        if not property:
            raise CustomException('Property does not exist')

        return property.tenants()
