import os

from dotenv import load_dotenv
from flask_migrate import (
    Migrate,
    MigrateCommand
)
from flask_script import (
    Manager,
    Shell,
    prompt_bool
)

from app import (
    create_app,
    db
)
from resources.models import (
    Property,
    Tenant,
    Unit,
    User
)

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

app = create_app(os.environ.get('FLASK_CONFIG') or 'development')
manager = Manager(app)
migrate = Migrate(app,db)


@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db, user=User, property=Property, tenant=Tenant, unit=Unit)

manager.add_command('shell', Shell(banner="Welcome to Property Management Shell"),
                    make_context=make_shell_context())

manager.add_command('db', MigrateCommand)

@manager.command
def dropdb():
    if prompt_bool("Are you sure you want drop databse"):
        db.drop_all()
        print("Database dropped")

if __name__ == "__main__":
    try:
        manager.run()
    except SystemError:
        print("An unexpected error occurred")