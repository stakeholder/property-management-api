import os

from flask import Flask
from flasgger import Swagger
from flask_sqlalchemy import SQLAlchemy

from config import config

configuration = config[os.environ.get('FLASK_CONFIG')]
db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    db.init_app(app)

    from api import api
    from resources import resources
    app.register_blueprint(api)
    app.register_blueprint(resources)
    Swagger(app)
    return app
