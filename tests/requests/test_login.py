import pytest
from . import TestBase


@pytest.mark.usefixtures('create_test_user')
class TestLogin(TestBase):
    def test_with_valid_credentials(self):
        # Arrange
        param = {'email': 'user@email.com','password': 'test'}
        # Act
        response = self.make_request('POST', '/login', payload=param)
        data = response.json
        # Assert
        assert response.status_code == 200
        assert data['success'] == True
        assert data['message'] == 'login successful'
        assert data['data'].get('token')

    def test_with_invalid_credentials(self):
        # Arrange
        param = {'email': 'user@email.com','password': 'wrong password'}
        # Act
        response = self.make_request('POST', '/login', payload=param)
        data = response.json
        # Assert
        assert response.status_code == 400
        assert data['success'] == False
        assert data['message'] == 'Invalid credentials'
