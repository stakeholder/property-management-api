from os import system

import pytest
from werkzeug.security import generate_password_hash

from app import create_app, db
from resources.models import User


@pytest.fixture(scope='module')
def create_test_app_instance():
    app = create_app('testing')
    app_context = app.app_context()
    app_context.push()
    yield app
    app_context.pop()
    print('done testing')

@pytest.fixture(scope='class')
def clear_users():
    User.query.delete()

@pytest.fixture(scope='class')
def create_test_user():
    User(email='user@email.com', password_hash=generate_password_hash('test'),
         firstname='sample', lastname='user').save()


