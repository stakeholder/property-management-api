import json
import unittest

import pytest
from flask import current_app


from app import create_app, db

@pytest.mark.usefixtures('create_test_app_instance')
class TestBase:
    _headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    def make_request(self, method, path, token=None, payload=None):
        if token:
            self._headers.update(token=token)
        data = json.dumps(payload)
        client = current_app.test_client()
        kwargs = {'data': data, 'headers': self._headers}
        return {
            'POST': client.post,
            'GET': client.get,
            'PUT': client.put,
            'DELETE': client.delete
        }[method](path, **kwargs)

