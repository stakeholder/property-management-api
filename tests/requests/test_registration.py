import pytest
from . import TestBase
from resources.models import User

@pytest.fixture(scope='class')
def clear_users():
    User.query.delete()

@pytest.mark.usefixtures('clear_users')
class TestRegistration(TestBase):
    def test_with_valid_parameters(self):
        # Arrange
        param = {
            'email': 'sample@email.com',
            'password': 'test',
            'verify_password': 'test',
            'lastname': 'Fox',
            'firstname': 'Megan'
        }
        # Act
        response = self.make_request('POST', '/register', payload=param)
        data = response.json
        # Assert
        assert response.status_code == 201
        assert data['data']
        assert data['success'] == True
        assert data['message'] == 'Operation was successful'
        assert data['data']['email'] == 'sample@email.com'
        assert data['data'].get('password') is None
        assert User.query.filter_by(email='sample@email.com').first()

    def test_with_invalid_parameters(self):
        # Arrange
        param = {}
        # Act
        response = self.make_request('POST', '/register', payload=param)
        data = response.json
        # Assert
        assert response.status_code == 400
        assert data['success'] == False

    def test_with_already_existing_email(self):
        # Arrange
        User(email='test@email.com', password_hash="werwetwrt",
             firstname='test', lastname='test').save()
        param = {
            'email': 'test@email.com',
            'password': 'test',
            'verify_password': 'test',
            'lastname': 'Fox',
            'firstname': 'Megan'
        }
        # Act
        response = self.make_request('POST', '/register', payload=param)
        data = response.json
        # Assert
        assert response.status_code == 400
        assert data['success'] == False
        assert data['message'] == 'this email test@email.com has been taken'

    def test_with_mismatch_passwords(self):
        # Arrange
        param = {
            'email': 'another_person@email.com',
            'password': 'test',
            'verify_password': 'testing',
            'lastname': 'Fox',
            'firstname': 'Megan'
        }
        # Act
        response = self.make_request('POST', '/register', payload=param)
        data = response.json
        # Assert
        assert response.status_code == 400
        assert data['success'] == False
        assert data['message'] == 'Password and Verify Password are not the same'
        assert not User.query.filter_by(email='another_person@email.com').first()



