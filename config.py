import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)


class Config:
    """
    This class is the base class for the different types of
    environment configuration
    """
    # gets the environment variable secret key from environment variable
    SECRET_KEY = os.environ.get('SECRET_KEY')
    ENV = os.environ.get('ENV')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SWAGGER = {
        'title': 'PROPERTY MANAGEMENT API',
        'uiversion': 3,
        'specs': [
            {
                'endpoint': 'apispec',
                'route': '/apispec.json',
                'rule_filter': lambda rule: True,
                'model_filter': lambda tag: True,
            }
        ],
        "static_url_path": "/flasgger_static",
        "specs_route": "/apidocs/index.html"
    }



class DevelopmentConfig(Config):
    """
    this is the development environment configuration, inheriting from the
    base configuration class and sets appropriate environment specific
    variables such as the database path and the debug value to True
    """
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URI')


class TestingConfig(Config):
    """
    this is the testing environment configuration, inheriting from the
    base configuration class and sets appropriate environment specific
    variables such as the database path for testing and the testing
    variable value to True
    """
    ENV = 'testing'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URI')

class ProductionConfig(Config):
    """
    this is the production environment configuration, inheriting from the
    base configuration class and sets appropriate environment specific
    variables such as the database path for production
    """
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

# setting all the various environment configuration into the config dictionary
config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig
}
