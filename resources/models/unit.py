from .base import (
    BaseMixin,
    tenant_units
)
from app import db


class Unit(BaseMixin, db.Model):
    __tablename__ = 'units'
    unit_type = db.Column(db.String(20), nullable=False, default='apartment')
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'), nullable=False)
    tenant_id = db.Column(db.Integer, db.ForeignKey('tenants.id'))
