from typing import (
    Dict,
    Any
)

from werkzeug.security import (
    generate_password_hash,
    check_password_hash
)

from .base import BaseMixin
from app import db


class User(BaseMixin, db.Model):
    __tablename__ = 'users'
    lastname = db.Column(db.String(20), nullable=False)
    firstname = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(30), unique=True, index=True, nullable=False)
    password_hash = db.Column(db.String(128))
    properties = db.relationship('Property', backref='user', lazy=True)
    tenants = db.relationship('Tenant', backref='user', lazy=True)

    @property
    def password(self):
        raise AttributeError("Password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    @classmethod
    def is_email_available(cls, email: str) -> bool:
        return not bool(cls.query.filter_by(email=email).first())

    @classmethod
    def create(cls, param):
        user_instance: cls = cls(email=param['email'], firstname=param['firstname'],
                                 lastname=param['lastname'])
        user_instance.password = param['password']
        user_instance.save()
        return user_instance

    def to_dict(self) -> Dict[str, Any]:
        result: Dict[str, Any] = {}
        for column in self.__table__.columns:
            result[column.name] = str(getattr(self, column.name))
        del result['password_hash']
        return result

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

