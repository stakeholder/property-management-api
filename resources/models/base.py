from datetime import datetime
from typing import (
    Dict,
    Any
)

from app import db


class BaseMixin(object):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow, default=datetime.utcnow())

    def save(self):
        db.session.flush()
        db.session.add(self)
        db.session.commit()

    @classmethod
    def update(cls, id: int , param: Dict[str, Any]):
        cls.query.filter_by(id=id).update(dict(**param))
        db.session.commit()
        return cls.query.get(id).to_dict()

    @classmethod
    def index(cls, filter_params: Dict[str, Any]):
        return [value.to_dict() for value in cls.query.filter_by(**filter_params)]

    def to_dict(self) -> Dict[str, Any]:
        result: Dict[str, Any] = {}
        for column in self.__table__.columns:
            result[column.name] = str(getattr(self, column.name))
        return result

tenant_units = db.Table('tenant_units',
                        db.Column('tenant_id', db.Integer, db.ForeignKey('tenants.id')),
                        db.Column('unit_id', db.Integer, db.ForeignKey('units.id')))
