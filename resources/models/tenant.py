from .base import (
    BaseMixin,
    tenant_units
)
from app import db


class Tenant(BaseMixin, db.Model):
    __tablename__ = 'tenants'
    lastname = db.Column(db.String(20), nullable=False)
    firstname = db.Column(db.String(20), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    units = db.relationship('Unit', backref='tenant', lazy=True)

    @classmethod
    def create(cls, param):
        tenant: cls = cls(firstname=param['firstname'], lastname=param['lastname'],
                                   user_id=param['user_id'])
        tenant.save()
        return tenant
