from typing import (
    Dict,
    Any
)

from .base import BaseMixin
from app import db
from .unit import Unit
from .tenant import Tenant


class Property(BaseMixin, db.Model):
    __tablename__ = 'properties'
    address = db.Column(db.String(20), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    units = db.relationship('Unit', backref='property', lazy=True)

    @classmethod
    def create(cls, param):
        property_instance: cls = cls(address=param['address'], user_id=param['user_id'])
        property_instance.units = [Unit(**value, user_id=param['user_id']) for value in param.get('units')]
        property_instance.save()
        return property_instance

    def tenants(self):
        return [Tenant.query.get(unit.tenant_id).to_dict() for unit in self.units]

    def to_dict(self) -> Dict[str, Any]:
        result: Dict[str, Any] = super().to_dict()
        result.update(units=[value.to_dict() for value in self.units])
        return result