# Property Management API
Property Management Service is a REST API that provides property related information.

## Development
* [Flask](http://flask.pocoo.org/) - Flask is a BCD licensed microframework for Python based on Werkzeug and Jinja 2.
* [Python-dotenv](https://github.com/theskumar/python-dotenv) - This library allows the saving of environment variables in .env and loaded when the application runs.
* [SQLAlchemy](https://www.fullstackpython.com/sqlalchemy.html) - This library is a Python distribution containing tools for working with sql databases.

## Installation
#### In Docker Environment
1. Start up your terminal (or Command Prompt on Windows OS).
2. Clone the repository by entering the command `git@gitlab.com:stakeholder/property-management-api.git` in the terminal.
3. Create a `.env` and `database.env` file in your root directory as described in `.env.example` and `database.env.example` file and configure the necessary options. It is essential you create this file before running the application.
4. Build a docker image by running the command `docke-compose up`
5. Enter into the python container and setup up your database following these steps: 
    * `python manage.py db init`
    * `python manage.py db migrate`
    * `python manage.py db upgrade`


#### Outside Docker Environment
1. Start up your terminal (or Command Prompt on Windows OS).
2. Ensure that you've `python 3` installed on your PC.
3. Clone the repository by entering the command `git@gitlab.com:stakeholder/property-management-api.git` in the terminal.
4. Create a `.env` and `database.env` file in your root directory as described in `.env.example` and `database.env.example` file and configure the necessary options. It is essential you create this file before running the application.
5. Install requirements by running the command `pip3 install -r requirements_dev.txt`
6. Start local server by running the command.
    * For unix systems:
      1. `export FLASK_APP=manage.py`
      2.  `flask run -h "0.0.0.0" -p 5000`  
    * For windows systems:
      1. `set FLASK_APP=manage.py`
      2.  `flask run -h "0.0.0.0" -p 5000`
7. Setup up your database following these steps: 
    * `python manage.py db init`
    * `python manage.py db migrate`
    * `python manage.py db upgrade`


### API Resource Endpoints
URL Prefix = Base URL is `http://localhost:5000` on your local system and `https://property-management-api.herokuapp.com` on heroku

Header format = `{"token": "eyJhbGciOiJIUzI1NiIsImV4cCI6MTQ5MzE5NDU3MCwiaWF0IjoxNDkzMTkwOTcwfQ.eyJpZCI6MX0.WNgqUeHjLxkzZtVOvuJmrG_OZts_iav5Q2Ogxz5VZR0", "Content-Type": "application/json"}`

`NB: you dont need to specify the token when accessing the register and login endpoints`


| EndPoint                                 | Functionality                 | Parameters|
| -----------------------------------------|-----------------------------|:-------------:|
| **POST** `/register`            | Registers a new user              |    `params = [email, password, verify_password, firstname, lastname]`    |
| **POST** `/login`        | Log into the system  |    `params=[email, password]`     |
| **POST** `/properties`           | Create a new property  |    `params=[address, units]` `nb: units should be a list in the format of [{"unit_type": "shop"}, {"unit_type": "apartment"}, {"unit_type": "saloon"}]`  |
| **PUT** `/properties/<int:property_id>`         | Update a property  |    `query_params=[property_id]`, `params=['address']`   |
| **GET** `/properties`       | Returns all properties created by logged in user       |       |
| **GET** `/properties/<property_id>/tenants`        | Returns a list of tenants in a property      |    `query_params=[property_id]`  |
| **POST** `/tenants`             | Create a new tenant |  `params=['lastname', 'firstname']`  |
| **PUT** `/tenants/<tenant_id>`                     | Update a tenant             |    `query_params=[tenant_id]`,   `params=['lastname', 'firstname']`    |
| **PUT** `/properties/<property_id>/units/<unit_id>/add`                 | Adds a tenant to a unit in a property              |    `query_params=[property_id, unit_id]`, `params=[tenant_id]`    |
| **PUT** `/properties/<property_id>/units/<unit_id>/remove`                   | Removes a tenant from a unit in a property    |    `query_params=[property_id, unit_id]`     |

## Authors

**Koya Gabriel.** - Software Developer at Andela

